Readme
------

This is a simple filter module to embed TeX-generated images into Drupal nodes. 
It uses <tex></tex> tags to automatically generate a PNG image based on the embedded
TeX code.  It also has rudimentary support for FCKEditor/HTMLArea integration.

Please refer to INSTALL for detailed installation instructions.

Send comments to drupal@malamas.com

